# What's this?

Design document for a custom wow server.

# What do I want from you

To estimate manhours/dollar costs of developing and maintaining the server.

For each feature, I assume 15% of costs are needed for core functionality and the remaining 85% are dedicated to polish and further bug squashing. In your estimations please note the 15% portion only.

I also want you to give commentary from a dev point of view. Things like:

* _"x is stupid/bad and here's why"_
* _"if we change x like this we can optimize performance"_
* _"x seems like it would be very costly to develop and I think should be removed or reworked from the ground up"_
* _"your assumption here is wrong - this is how this works"_

Please also give answers to questions I didn't ask but should have.

I would be also happy if you could point out any inconsistencies in the systems described and to tell me whether you like the ideas. But it's not a requirement.

# Contents

* [Engine](#engine)
* [Island](#island)
  * [Manual setup](#manual-setup)
  * [Initialization](#initialization)
  * [Timeout](#timeout)
* [Island modules](#island-modules)
  * [Base](#base)
    * [Base flagpole](#base-flagpole)
    * [Base control mechanics](#base-control-mechanics)
    * [Basekeeper NPC](#basekeeper-npc)
    * [Tent space](#tent-space)
      * [Tent](#tent)
      * [Tent space access control](#tent-space-access-control)
    * [Spawn point](#spawn-point)
    * [Firepit](#firepit)
    * [Workspace](#workspace)
    * [Communal base](#communal-base)
  * [Waygates](#waygates)
  * [Waypoint system](#waypoint-system)
  * [Mobs, Elites, Bosses](#mobs-elites-bosses)
  * [Environmental hazards](#environmental-hazards)
  * [Chests](#chests)
  * [Flagpole](#flagpole)
  * [Sanctuary space](#sanctuary-space)
  * [Other modules](#other-modules)
* [Island types](#island-types)
  * [Communal Island](#communal-island)
  * [Island with claimable bases](#island-with-claimable-bases)
  * [Wilderness](#wilderness)
* [Gameworld](#gameworld)
  * [Island initialization system](#island-initialization-system)
    * [Connections between islands](#connections-between-islands)
  * [Dynamic resource generation/distribution, professions](#dynamic-resource-generationdistribution-professions)
* [Transit](#transit)
  * [Transit rules](#transit-rules)
    * [Transit party rule](#transit-party-rule)
    * [After-jump effect](#after-jump-effect)
  * [Transit modes](#transit-modes)
    * [Waygate system](#waygate-system)
    * [Waygates in communal islands](#waygates-in-communal-islands)
    * [Involuntary teleport](#involuntary-teleport)
    * [Hearthstone](#hearthstone)
    * [Summon](#summon)
* [Other settings and mechanics](#other-settings-and-mechanics)
  * [Global settings](#global-settings)
  * [Character death](#character-death)
    * [Base binding](#base-binding)
  * [Asset loss](#asset-loss)
    * [Asset destruction/drop](#asset-destructiondrop)
    * [Partial asset destruction](#partial-asset-destruction)
  * [Soulbound items](#soulbound-items)
    * [Talisman](#talisman)
  * [Cozy Fire](#cozy-fire)
* [Miscellaneous questions](#miscellaneous-questions)
* [Other project costs](#other-project-costs)

---

* [Character mechanics](#character-mechanics)
  * [Primary and secondary attributes](#primary-and-secondary-attributes)
    * [Strength](#strength)
    * [Agility](#agility)
    * [Intellect](#intellect)
    * [Stamina](#stamina)
    * [Spirit](#spirit)
  * [XP system](#xp-system)
    * [Talent system](#talent-system)
  * [Other stats and mechanics](#other-stats-and-mechanics)
    * [Rage](#rage)
    * [Energy, Focus](#energy-focus)
    * [Pushback](#pushback)
    * [Stealth](#stealth)
* [Races and classes](#races-and-classes)
  * [Racial stats](#racial-stats)
  * [Class stat modifiers](#class-stat-modifiers)
  * [Racials](#racials)
    * [Human](#human)
    * [Blood Elf](#blood-elf)
    * [Draenei](#draenei)
    * [Dwarf](#dwarf)
    * [Gnome](#gnome)
    * [Night Elf](#night-elf)
    * [Orc](#orc)
    * [Tauren](#tauren)
    * [Troll](#troll)
  * [Race/Class combos](#raceclass-combos)
* [Miscellaneous character mechanics ideas](#miscellaneous-character-mechanics-ideas)
  * [Starting abilitites by class](#starting-abilities-by-class)
  * [Rogue](#rogue)
  * [Priest](#priest)

# Engine

_What engine is the most suitable? (I was thinking azerothcore) See_ [_Gameworld_](#gameworld) _for what I assume is the most relevant info to this._

_Is it better to take a certain version of engine and develop everything on that one version, or to continually keep everything up-to-date with newer versions of the engine?_

# Island

Islands are where players live and play. The entire gameworld is composed of islands only.

![Zone_layout_Moonglade](Zone_layout_Moonglade.png)

_Quick and dirty mockup of an island overlaid over Moonglade. Not to scale. Size, proportions, distances will be adjusted based on playtests. Geometric shapes within Farm area are an example of module placement. These modules can be filled with mobs (yellow outline is aggro range), environmental hazards, serve as sanctuary etc._

## Manual setup

1. **MAP** Take/Adjust/Make a map.
   * Size: One island houses up to 49 people. There are no mounts. Islands with heavy activity may have <span dir="">\~</span>150-200 people in them.
   * _How much does it cost to take an ingame map with size roughly as Moonglade and adjust it so all outer boundaries are highly polished cliffs, seas or other definite spatial breaks?_
   * _How much would it cost to produce highly polished map the size of Moonglade instead completely from scratch?_
     * _Is it needed to do any additional programming for custom maps? Pathing grid or something like that..?_
       * _how about skyboxes, general light color palette, shadows...?_
2. **MODULES** Manually place selected modules on the map and if needed, do any additional programming.

## Initialization

* **INITIALIZATION** The prepared map with pre-placed modules will initialize itself as instance based on embedded instructions in modules. Resource nodes will be populated, mobs will spawn, connections with other instances will be made etc. All of this done without any manual effort. One map can be initialized into as many instances as needed. Each instance will presumably look the same, but the resources, mobs etc. may be different, based on variables in the modules used.

also see [Island initialization system](#island-initialization-system)

## Timeout

All newly initialized island instances except [Communal Islands](#communal-island) start with (tentatively 7 days) remaining time. When remaining time reaches 0 instance will despawn, killing everyone inside, despawning all tents etc.

Each creature killed by a player extends island duration by (tentatively 30 minutes). Group of 21 dedicated players (3 guilds) should be able to keep their home island on perpetually just by being active in it.

Timeout mechanic serves to clean up unused/underpopulated instances. For instance creation, see [Island initialization system](#island-initialization-system). Together, these two systems continually adjust available playspace to active players.

As server progresses islands will depopulate due to players losing interest in the game. Remaining players, even if they are active, ultimately won't be able to keep the islands with their homebases on. It is **_crucial_** to explain to players as soon as feasible to expect this and let them know solution is to move to other island. Otherwise players will feel game is punishing them for something which is not their fault and will eventually quit.

# Island modules

## Base

Is a circle with (tentatively x-yard) radius. Boundary is roughly demarcated by terrain features.

* serves as home, respawn point for players
* by default can be claimed by a guild and house up to 7 characters
* is a friendly territory for all player characters bound to base
  * characters entering homebase will become blue (but any debuffs they may have are not cleared)
    * players can attack other players from the safety of their homebase with impunity
    * mobs will leash if they receive any damage from character they cannot hit back
* has permanent aura which breaks any kind of stealth or invisibility

Usually there are 7 bases located on one island. All characters bound to the same base are always green to each other.

![Base_layout](Base_layout.png)

_Base layout draft. Not to scale. Flagpole is in the center. Tent space, Workspace, Basekeeper, Spawn point are all areas in which players interface with the game and are clumped relatively close together. Firepit is designed as focal point for downtime player-to-player interaction. Tent space and Firepit area are partially enclosed by small cliffs, body of water, copse of trees etc._

Base placement follows these rules:

* /say anywhere within a base and its immediate boundary won't be heard in any other base
* /yell anywhere within a base can be heard within 1 or 2, but no more than 3 other bases  
* there's always respectable distance between any base and waygate areas
  * generally speaking if all else is equal, defenders (base denizens) have slight-to-moderate advantage in island area control
    * and massive advantage when in vicinity of their homebase
  * well-prepared travelers/convoys should be able to traverse any island with reasonable expectation of success 

#### Base flagpole

Located at center of base. If a base is claimed, there's flag hoisted with guild tabard design on it.

#### Base control mechanics

_I was thinking existing guild code would serve well for base management....? Base can by claimed by turning in filled guild charter to basekeeper npc. Guild dissolution frees the base for claiming by some other guild._

Turn in guild charter with at least 5 signatures (including guildmaster's) to Basekeeper NPC to claim a claimable base.

If at any point amount of characters in guild or amount of deployed tents is 2 or less, trigger [Involuntary Teleport](#involuntary-teleport) on all guild members and disband the guild. This will remove any leftover tents, remove flag from flagpole etc. -> will make base claimable again.

Guilds can only have 7 members at most, same as the maximum housing capacity of a base.

#### Basekeeper NPC

Located between Tent space and Workspace, close to base boundary.

* is green to everyone (even characters which are not bound to the base)
* serves as interface for base management
  * if the base is unclaimed and claimable, hands out and will accept guild charter
  * will explain base and island mechanics, answer frequently asked questions
  * gives [talisman](#talisman) upon asking
* offers vendor service
* lots of gossip
  * _any experience with people being paid for writing lore? how much do you think 100 paragraphs of high-quality gossip would cost to produce?_

#### Tent space

Roughly oval shape large enough to house up to 7 tents.

#### Tent

* serves as personal bank and a way to manage where character is bound to (see [Base binding](#base-binding), [Spawn point](#spawn-point))
* is deployed by standing at the tent space of your homebase and _casting the tent spell from spellbook...?_
* act of deploying tent [binds character to base](#character-death)
* only one tent per character
* casting tent spell while old tent is deployed somewhere will fizzle the spell

#### Tent space access control

* new player joins guild
  * can now deploy their tent in guild tent space
    * do not bind character to new base on joining (otherwise player can hearthstone and sidestep asset loss mechanism)
* kicking a guild member will:
  * trigger [partial asset destruction](#partial-asset-destruction) on character which issued the command (including their tent)
  * trigger [partial asset destruction](#partial-asset-destruction) on pariah (including their tent) and their tent despawns
  * trigger [Involuntary teleport](#involuntary-teleport) on pariah
* if guild member leaves:
  * trigger [partial asset destruction](#partial-asset-destruction) on pariah (including their tent) and their tent despawns
  * trigger [Involuntary teleport](#involuntary-teleport) on pariah

#### Spawn point

* located between Tent space and Flagpole, close to Tent space
* after player releases spirit or uses [talisman](#talisman) their character respawns/teleports at spawn point
  * also see [Base binding](#base-binding)

#### Firepit

(not to be confused with [Cozy Fire](#cozy-fire))

* Campfire gives xp rested bonus to all guildmembers present in base (logged-off too)
* costs resources to deploy and gives special bonus (...what kind..?) to whoever deployed

#### Workspace

Located close to Tent space. Place for crafting, where profession benches are/can be deployed.

### Communal base

These are placed in [Communal Islands](#communal-island).

* cannot be claimed
* do not have tent space
* no firepit

## Waygates

See [Waygate System](#waygate-system)

## Waypoint system

Some kind of system which will enable mobs, npcs, elites to move around island without having to manually waypoint them. Perhaps something like?..:

1. creature is spawned or arrived at destination point, select randomly:
   * x % chance to stay for x sec, then return to 1.
   * x % chance to select point X as next destination
     * start walking to X, once there return to 1.
   * x % chance to select point Y as next destination
     * start walking to Y, once there return to 1.

_-> this would require to manually select neighbouring points for each point and when placing the points on map to make sure all paths are walkable, do not cross any bases etc.... any better way to do this?_

## Mobs, elites, bosses

_How easy is to introduce randomness and variability into mob area spawns? Let's say we make module where we demarcate area on map and specify type (let's say Undead) and difficulty and this will be all that's needed to fully initialize area filled with mobs and elites which have abilities, filled loot tables, wander around etc... how much would this cost?_

_How easy is to modify then place vanilla dungeon/raid bosses into open world? Or some other low-cost way to have bosses in the world..?_

_How does mob ability selection work in engine?_

_How much would it cost to create 7 mobs (each mob has a name and interesting setup of hp, damage, abilities) then write function which will spawn a randomized pack, with randomized number of these mobs on a waypoint?_

## Environmental hazards

_How costly would be to make module which will create effect upon demarcated area? Effects for example - increase/decrease movement speed, apply periodic damage etc. (...these areas have to be somehow visually demarcated..)_

## Chests

Come in several varieties:

* unlocked
* can be opened only by one-use Skeleton Keys made by blacksmiths
* can be opened only by one-use Seaforium Charges made by engineers
* can be opened only by lockpicking (...this is Rogue-exclusive profession or...?)

## Flagpole

Whoever successfully channels for 10 sec will get friendly territory for themselves and all their guildmates in nearby small area.

Flagpoles provide temporary shelters/forward operating bases in [Wilderness](#wilderness).

## Sanctuary space

Small areas in [Wilderness](#wilderness) which make anyone within blue to anything.

## Other modules

_Generally speaking, besides mobs, elites, resource nodes, chests, what else on an island can be variable? How do objects work, what are the limits regarding them? What kind of modules can be made to make islands more interesting and with more variety? Can the maps themselves be somehow variable? What are some low-cost ways to enable players to customize their living space and neighbourhood?_

# Island types

## Communal Island

These serve as starting islands and also destination for [Involuntary teleport](#involuntary-teleport).

* all characters are red to each other by default (characters bound to the same base and in party are green to each other)
* no timeout mechanic
* have Communal Bases
* no soloable mobs, have to group up right from start
* mobs drop basic equipment in relative abundance, also some basic profession recipes
  * otherwise resources are very rare
* special rules for [Waygates](#waygate-system)
* are identical to each other and have mechanic which during downtime despawns emptying islands and rebinds characters from them into other communal islands
  * when rebinding, always move all characters from one old base to one new base - never split old group of characters into several new bases

## Island with claimable bases

Most numerous type of island with 7 claimable [bases](#base).

## Wilderness

* no bases present (only [Flagpoles](#flagpole) and [Sanctuaries](#sanctuary))
* resources/loot in wilderness is finite - will stop spawning after some time (days)
  * function which reduces amount of dropped loot with passage of time - how costly to implement?

(....idea for solo playstyle/profession - which allows people to roam wilderness - they cannot join a guild, after death always respawn at some random [Sanctuary space](#sanctuary-space) - respawn algorithm prefers to spawn characters in the same destination for some time so they can group up right away /mobs are not soloable/)

# Gameworld

## Island initialization system

_I'm assuming the best way is to do something like this?:_

1. _Determine online player peak one server can reliable handle_
   * _one_ [_island with claimable bases_](#island-with-claimable-bases) _can house up to 49 characters (7 guilds)_
   * _islands can have 1 - 3 active connections (except communal islands which have only one one-way, outbound teleport)_
     * _island vs island is <span dir="">\~</span>100 characters, 3-way fight is <span dir="">\~</span>150 characters in one instance_
     * _wilderness instance with great resources and 3 active connections can attract 200+ characters_
       * _200-character hard limit in all instances?_
     * _maybe <span dir="">\~</span>40% of islands will have less than 50 characters in them at any time...?_
   * _if we make long connection chains of wilderness instances only, the outlying instances will be likely near-empty (how much of system resources would these near-empty instances take?)_
   * _what's the likely bottleneck - CPU, memory, something else? How will system resources be affected by reducing/increasing island map space? etc.._
   * _...scheduled server maintenance/downtime probably once per 24 hours..?_
2. _Establish maximum number of islands which can be active at once_
3. _Divide islands into resource regions_
4. _Write algorithms which will work in concert:_
   * _When players trigger new instance initialization (_[_Waygates in communal islands_](#waygates-in-communal-islands)_) Island initialization system will determine from which resource region should a new island be initialized_
   * _connections to this newly initialized instance will be made according to internal rules and needs_
     * _for example usually each island from the same resource region has at least one connection with some other island from the same resource region_

### Connections between islands

Minimum one, up to three, usually two. See [Waygate system](#waygate-system).

## Dynamic resource generation/distribution, professions

There are (tentatively 3) resource regions:

* jungle (Stranglethorn Vale)
* forest-fall (Azshara)
* savannah (Barrens)

_(...3 resource regions means at least 3 base maps...)_

Each island belongs to at least one resource region. Each region has its own resource table. Each island in the same resource region usually shares at least one connection with at least one other island in the same region. Region resource tables generally follow this format:

* one third of all resource types existing in game is relatively commonplace in all islands in a given region
* one third is scarce
* one third is not present in region at all

ff
|  | Abundant | Scarce | Not present |
|--|----------|--------|-------------|
| metal 1 | jungle | force | savannah |
| metal 2 |  |  |  |
| metal 3 |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |

Each island can also add something on top of its regional resource table, for example selecting one resource from _relatively commonplace_ part of table and spawning +33% more of it for x days.

Possibly each island can also have its resource table modified by connections it has: for example island in mountain region having connection to plains region will spawn some plains mobs/resources too.

Some wilderness islands can belong to two resource regions, making them desirable for farming and presumably acting as hotspots of player activity.

Possibly general rule: Resources in islands with claimable bases continually respawning but resources (including loot) in wilderness are always time-limited.

Gameplay aim is to create local scarcity and force players to travel for trade and plunder. For example:

* if there's lots of copper in one island chances are there will be lots of copper in adjacent islands too
  * but as one travels further and further away copper will become more and more scarce
* crafting full set of good equipment is not possible if resources from only one region are utilized

_How does node placement and generation on a map work? Hard-coded places where nodes can spawn and algorithms governing what will spawn where?_

_Generally speaking, to implement professions one does have to do mostly database work? ...Basically input master templates...?_

#### Miscellanous profession ideas:

...Freshly cooked food which can be made at any fire which gives good buffs. Has to be consumed within x minutes after being made.

* Limited recipes
  * mobs drop recipes which can be cast only x times and after that will be destroyed
    * for example make only 10 copies of an axe
* Profession benches
  * can be deployed by players in designated areas? can players choose which bench to deploy? specialized benches which allow for forging of advanced recipes but cost resources to deploy and have limited duration?
* tbc quest [Triangulation Point](https://tbc.wowhead.com/quest=10269/triangulation-point-one) where you use an item and it turns your character towards coordinates
  * can this be implemented for resource surveying?
* several enchants on piece of equipment - possible, performance impact? names on enchanters along with the enchants they made on equipment? spells which have only limited amount of uses? or have to be items
* (Idea: when looting a resource node, auto-share loot with all other friendly characters which would be able to gather from that node (skinning too)).
* _what other profession ideas and implementations have sprung up?_

# Transit

## Transit rules

### Transit party rule

Players in party teleporting from the same origin point within 4 minutes of the first party member teleporting will always arrive in the same destination. _When the first party member teleports, a snapshot of all party members is made? and players joining or leaving party after this are not included_

### After-jump effect

After appearing in destination upon using any kind of transit mechanic character gets two effects:

1. Buff which grants enhanced stealth
   * ends after 30 sec or as soon as character moves, speaks or uses any ability
2. Debuff which prevents use of any kind of transit
   * 1 min duration

## Transit modes

### Waygate system

Connects islands.

Waygate is an unobtrusive circle placed on ground but visible from certain distance (perhaps similar visual effect as when a flag in Arathi Basin is being captured) which when walked upon will teleport character to target island. 21 waygates are placed on any given island (except [Communal Islands](#communal-island)...?) in 3 groups of 7. Waygates from different groups are differentiated by color. Both sides of connection have the same color. Walking over a connection will teleport player to destination island to a randomly selected spot in area near waygates of the same color. Waygates follow [Transit party rule](#transit-party-rule).

![Waygate_system](Waygate_system.png)

_Rough drawing, not to scale. Character on island A stepping over one of green Waygates will get teleported anywhere within green area on island B. If they want to get back to island A, they have to walk over any green waygate and will be teleported in the same fashion (randomly selected spot anywhere in green area). The other waygates in their respective groups are paired with their counterparts on different islands._

_Alternatively in stead of waygates there can be thin, wide outbound teleport line placed along the edge of instance._

### Waygates in communal islands

Work differently:

* are one-way
* only one per island
* (10 sec cast time...)

_(Following is just a rough idea of how this could work ...works in concert with_ [_Island Initialization System_](#island-initialization-system)_. Design intention is to place players from communal islands into islands where they can set up their bases or meet guilds which are looking for new members)_ Teleport character(s) to random destination island from a pool selected by these criteria:

1. is a [Island with claimable bases](#island-with-claimable-bases)
2. amount of characters bound to local bases is more than x and less than y
3. has at least 6 days remaining time
4. at least z characters active in the last a minutes in island

Destination pool empty? Initialize an island and teleport character(s) there.

Cannot initialize new island because max amount of initialized instances has been reached? Select random Island with claimable bases and teleport character(s) there.

### Involuntary teleport

Used anytime character needs their base bind changed or doesn't have any. Destination can only be [Communal Island](#communal-island). Always binds character to destination base. Characters in the same party/guild going through Involuntary teleport will always be bound to the same destination base. Structured in a way to try to fill each communal base with 7 online players (striving for communal island with 49 online players).

Triggered in these cases:

* newly-created character enters world for the first time
* characters lost their base
  * by having 2 or less members in guild/2 or less tents deployed in their guild base
  * to island timeout
* character kicked from or leaves guild

Algorithm will search for suitable Communal Island (not too empty, not too full) and if needed, will create new one. _(can be also used to spawn the first island when the very first character on a server enters world for the first time?)_

_(merge this with Communal Island function which rebinds characters from underpopulated communal islands?)_

### Hearthstone

Teleports character to their home base.

go to [Talisman](#talisman)

### Summon

Serves to get together people which know each other outside of game

_(..is there any way this mechanic can be gamed for RMT, easy shopping in trade hubs or similar behaviour?)_

_(likely too many avenues for abuse, and maybe harms the social aspect of game - this mechanic probably won't be included at all)_

* castable by anyone
* can be cast only on guildmate (do this check second time right before actual teleport)
* channel only possible from homebase
* triggers [asset partial destruction](#asset-partial-destruction) on summonee
* will fizzle if island remaining time less than 3 days
* summonee has to go through double UI confirmation to be sure they understand half of their things will be destroyed

# Other settings and mechanics

## Global settings

* only one character per account
* game balanced around level 29 (see [XP system](#xp-system))
* item durability and repair system disabled
* gold can be carried on person or put into [tent](#tent) and is affected by [asset loss](#asset-loss)
* only these chat channels are enabled:
  * say (...vanilla default range is 25 yards...)
  * yell (...vanilla default range is 300 yards...)
  * emote
  * party
* races are not divided into Horde, Alliance factions
* can duel, trade with hostile characters which are not in combat (via slash command or right clicking portrait)
* maximum party size is 7 and raids are disabled
* maximum guild size is also 7 (guild charter needs at least 5 signatures including guildmaster and is acquired from and turned in to any [Basekeeper](#basekeeper-npc))
* exiting game when not in your [base](#base) will leave your character logged-in in the world for up to (x minutes)
* resurrection timer cap set to 180 seconds (...vanilla is 120 seconds...)

## Character death

After player releases spirit, their character is respawned at [base](#base) it's bound to.

### Base binding

Upon entering game for the first time, character goes through [Involuntary teleport](#involuntary-teleport). This will bind them to a communal base. Player can voluntarily change location of their character's spawn point by [deploying a tent](#tent-space).

## Asset loss

### Asset destruction/drop

50% of gold, items in containers, equipped items are destroyed. The other half drops as loot.

Triggers _on character only_ when:

* character dies

### Partial asset destruction

50% of gold, items in containers, equipped items are destroyed. The other half remains as it was.

Triggers _on character only_ when:

* character is in instance which timeouts
* character accepts [Summon](#summon)

Triggers _on tent only_ when:

* tent is in instance which timeouts
  * _(casting tent spell while old tent is deployed somewhere will fizzle the spell)_

Trigger _on both character and their tent_ when:

* character leaves or is kicked from a guild
* character kicks other character from a guild
* guildmembers lose their base
  * by island timeout or
  * guild disband due to 2 or less characters remaining in guild

_(...wording is bad here... asset loss is not supposed to trigger several times in short succession - for example guild disband by island timeout is not supposed to trigger asset loss on tent 2 times but only once)_

## Soulbound items

* never affected by [asset loss](#asset-loss) system
* vendors don't want them
* cannot be traded nor enchanted
* only way to get rid of soulbound item is to destroy it manually by dragging it out of inventory space
* items which are soulbound by default:
  * (tent is spellbook spell...?)
  * [talisman](#talisman)
  * starting bags
* items which can be made soulbound via some kind of upgrade/ritual:
  * cosmetic wearables
  * bags with more item slots

### Talisman

* replaces hearthstone
* equippable as neck, has 3 gem slots
* [soulbound](#soulbound-items), [Basekeeper](#basekeeper-npc) gives new one upon asking _(implement confirmation dialog because new talisman will wipe the old, even if the old has been gemmed)_
* castable from anywhere
* 10sec cast time, 8? min cooldown
* returns you to spawn point of base you are bound to

## Cozy Fire

Can be deployed by anyone at no cost. Only one cozy fire per character. (large cooldown to curtail spamming?) Restores 3% of hp and mana per sec to anyone nearby and not in combat.

# Miscellaneous questions

* possible future localization - how to accomodate it in code? in front of every blurb which needs to be translated, add comment with common string followed by unique identifier? this will make extracting/putting back in pieces of what needs to be translated easy?

# Other project costs

* Server maintenance
* Website, account system
* Custom login and character creation screens
* Lore
* what have I forgot?

---

---

# Character mechanics

Budget alternative is to leave vanilla engine as is and tweak [racials](#racials), talents and ability numbers to fit to level 29.

Ideally we are aiming for complete overhaul:

## Primary and secondary attributes

Formulas are the same for all classes.

### Strength

* **Melee Attack Power** (Str+Agi)/2
* **Melee Critical Damage** <span dir="auto">1.43+(Str\*0.0033</span>)
* **Block damage mitigation** _??_

### Agility

* **Melee Critical Chance** <span dir="auto">Agi/(Agi+400)\*100</span> _(formula needs to be modified, crit chance too high - should be no more than 15% at 150 Agi)_
* **Ranged Attack Power** (Agi+Spi)/2
* **Ranged Critical Damage** <span dir="auto">1.43+(Agi\*0.0033</span>)
* **Dodge** _??_

### Intellect

* **Spell Critical Damage/Healing** <span dir="auto">1.43+(Int\*0.0033</span>)
* **Mana Pool** (base mana pool...?)
* **Spell Power** (Int+Spi)/2

### Stamina

* **Health** <span dir="auto">20+(Sta-20)\*10</span> _(this I think is the default formula ...probably make HP pools overall lower due to reduced occurence of crits...?)_

### Spirit

_(..rename to Wisdom?)_

* **Spell Critical Chance** <span dir="auto">Spi/(Spi+400)\*100</span> _(formula needs to be modified, crit chance too high - should be no more than 15% at 150 Spi)_
* **Mana Regen** <span dir="auto">Spi\*0.06\*Int^0.14</span> _(formula should be good enough for initial testing)_
* **Ranged Critical Chance** <span dir="auto">Spi/(Spi+400)\*100</span> _(formula needs to be modified, crit chance too high - should be no more than 15% at 150 Spi)_

## XP system

Unlike vanilla, levels do not provide stats and do not make it harder for lower level characters to hit higher level characters. On level-up character gets 1 talent point.

### Talent system

Vanilla talent trees are modified:

* abilities are included in talent trees (no trainers)
* all abilities and talents are divided into ranks:
  * Apprentice
    * each class gets 3-5 starting abilities by default
  * Journeyman
    * no prerequisites
    * 1 Journeyman ability costs 1 point to learn
  * Expert
    * requires at least 3 points from lower rank (Journeyman) in the same tree
    * one Expert ability costs 2 points to learn
      * (prerequisites + first Expert ability = 5 points)
  * Artisan
    * requires at least 7 points from lower ranks (Journeyman, Expert) in the same tree
    * one Artisan ability costs 3 points to learn
      * (prerequisites + first Artisan ability = 10 points)
  * Master
    * requires at least 10 points from lower ranks (Journeyman, Expert, Artisan) in the same tree
    * one Master ability costs 4 points to learn
      * (prerequisites + first Master ability = 14 points)
* there are 15 talent points total
* at some points players can unlock second talent board
  * switching between these two talent boards is relatively inexpensive
  * would make for 30 levels total
* mobs drop ability books which teach rank 2 of each existing ability
  * most books are freely tradeable except books for Artisan and Master abilities which are BoP

![mage_talent_tree_draft](mage_talent_tree_draft.png)

_Draft of mage talent tree. More saturated panels contain active abilities, less saturated passives. Abilities in light orange panels can count towards points in more trees than one._

## Other stats and mechanics

### Rage

* devise and implement new rage formula which makes rage generation consistent regardless of weapon used or damage output ...only Haste should increase rage generation?

### Energy, Focus

* _Feasible to merge Focus into Energy? Or is it better to leave Focus with its slower tick rate to save server resources?_
* 20 Energy per tick, tick = 2 sec (same as in vanilla)

### Pushback

* 0.6 sec first hit
* 0.4 sec second hit
* 0.2 sec third hit
* 0.1 sec fourth and any additional hits

### Stealth

Permant stealth impossible. Stealth either drains energy/mana or has limited duration in seconds.

# Races and classes

We are aiming for roughly this stat weight:
|  |  | % |
|--|--|---|
| Race | 20 | <span dir="">\~</span>13% |
| Class | <span dir="">\~</span>30 (coming from talents mostly) | <span dir="">\~</span>20% |
| Equipment | <span dir="">\~</span>45 (worst) to <span dir="">\~</span>70 (best) | <span dir="">\~</span>30% - <span dir="">\~</span>45% |
| Buffs | up to <span dir="">\~</span>45 | <span dir="">\~</span>30% |
| _total_ | <span dir="">\~</span>150 (primary stat upper value) |  |

## Racial stats

_(same as level 1 in wotlk)_
|  | Strength | Agility | Intellect | Stamina | Spirit |
|--|----------|---------|-----------|---------|--------|
| Human | 20 | 20 | 20 | 20 | 20 |
| Blood Elf | 17 | 22 | 23 | 20 | 18 |
| Draenei | 21 | 17 | 20 | 20 | 22 |
| Dwarf | 25 | 16 | 19 | 21 | 19 |
| Gnome | 15 | 22 | 23 | 20 | 20 |
| Night Elf | 16 | 24 | 20 | 20 | 20 |
| Orc | 23 | 17 | 17 | 21 | 22 |
| Tauren | 25 | 16 | 16 | 21\* | 22 |
| Troll | 21 | 22 | 16 | 20 | 21 |

_\*Tauren racial not included_

## Class stat modifiers

_(same as level 1 in wotlk)_
|  | Strength | Agility | Intellect | Stamina | Spirit |
|--|----------|---------|-----------|---------|--------|
| Druid | +1 |  | +2 |  | +2 |
| Hunter |  | +3 |  | +1 | +1 |
| Mage |  |  | +3 |  | +2 |
| Paladin | +2 |  |  | +2 | +1 |
| Priest |  |  | +2 |  | +3 |
| Rogue | +1 | +3 |  | +1 |  |
| Shaman | +1 |  | +1 | +1 | +1 |
| Warlock |  |  | +2 | +1 | +2 |
| Warrior | +3 |  |  | +2 |  |

## Racials

_(...all active racials have the same long cooldown?.. ...8min?)_

### Human

* (non-combat, profession perk)
* passive: character and each non-human party member within sight gets +1 to all stats which aren't their primary class stats _(too OP?)_
* active pvp ability which represents human ability to be versatile with other races
  * bonus to crit of next ability but increases all dmg received for x sec?
    * movement speed? damage reduction? limited pvp trinket effect?
  * gives boon to both caster and friendly target....?
    * less powerful boon to caster and more powerful boon to target?
      * human targets no boon? or less powerful boon compared to other races?

### Blood Elf

* <span dir="">\~</span>6% armor penalty
* bonus spell penetration
* bonus resistance
* +1% crit chance
* **<span dir="">_Arcane Torrent_</span>**<span dir="">: _Silence all enemies within 8 yards for 1 sec, remove 1 beneficial effect from each and restore 4% of your Mana/15 energy._</span> (long cldwn)

### Draenei

* <span dir="">select buffs cast by draenei are slightly better</span> _(for this, I assume it is required to create completely new set of buffs with modified numbers just for draenei?)_
* [**<span dir="">Perception</span>**](https://wotlkdb.com/?spell=58985)<span dir="">: Increases your Stealth detection.</span> (..wotlk default is +5... change to +10)

### Dwarf

* better block
* **Might of the Mountain**: Critical strike damage bonus with melee weapons and guns increased by 3%.
* **<span dir="">_Stoneform_</span>**<span dir=""> (triggers pvp trinket cooldown)</span>: <span dir="">_Removes all harmful Poison, Disease, and Bleed effects and reduces all physical damage taken by 10% for 8 sec._</span> (long cldwn)

### Gnome

* <span dir="">\~</span>6% armor penalty
* exclusive access to 2 or 3 (or most if not all?) gnomish engineering recipes/items (which can be used only by gnomes)
* some kind of minor active mage-themed ability?
* **Nimble Fingers**: Haste increased by 1%.
* [**<span dir="">_Escape Artist_</span>**](https://classic.wowhead.com/spell=20589): <span dir="">_Escape the effects of any immobilization or movement speed reduction effect._ </span>(0.5sec cast, 1.5sec gcd., long cldwn)

### Night Elf

* **Touch of Elune**: <span dir="">Increases your Haste by 2% during the night. Increases your Critical Strike chance by 2% during the day</span>.
* **Quickness**: Increases your chance to dodge melee and ranged attacks by 2%, and your movement speed by 2%.
* **Elusiveness**: <span dir="">Increases your movement speed by 15% while </span>stealthed and makes you more difficult to detect while stealthed (+5 to stealth level).

### Orc

* resistance penalty
* <span dir="">bonus physical penetration, for pets too</span>
* bonus to originating Fear effects?
* **<span dir="">Hardiness</span>**<span dir="">: Stun duration reduced by an additional 15%</span>.
* **<span dir="">_Blood Fury_</span>**<span dir="">_:_ (+power, -healing received)</span> (no gcd, long cldwn)

### Tauren

_(In wotlk client, how are animations of non-playable races? Would it be feasible to replace taurens with some other similar race?)_

* **<span dir="">Endurance</span>**<span dir="">: flat +5 Stamina</span>
* <span dir="">Cultivation (herbalism bonus)</span>
* [**<span dir="">_War Stomp_</span>**](https://wotlkdb.com/?spell=20549)<span dir=""> (0.5 sec cast, 2 sec stun - exactly the same as in wotlk but long cldwn)</span>

### Troll

* penalty to fire resistance
* [**Troll Regeneration**](https://tbc.wowhead.com/spell=16492/blood-craze)<span dir="">: _When you take damage, heal for 4% of that amount over 4 sec._</span>
* **<span dir="">_Berserking_</span>**: <span dir="">_Increases your haste by 10% to 30%.  At full health the speed increase is 10% with a greater effect up to 30% if you are badly hurt when you activate Berserking._</span> <span dir="">_Lasts 8 sec._</span> (no gcd, long cldwn)

_(Undead not a playable race)_

## Race/Class combos

If possible, restrict night elf priests to female only.
|  | Druid | Hunter | Mage | Paladin | Priest | Rogue | Shaman | Warlock | Warrior |
|--|-------|--------|------|---------|--------|-------|--------|---------|---------|
| Human |  |  | Y | Y | Y | Y |  | Y | Y |
| Blood Elf |  | Y | Y |  | Y | Y |  | Y |  |
| Draenei |  |  | Y | Y | Y |  |  |  | Y |
| Dwarf |  | Y |  | Y | Y | Y |  |  | Y |
| Gnome |  |  | Y |  |  | Y |  | Y | Y |
| Night Elf | Y | Y |  |  | Yf | Y |  |  | Y |
| Orc |  | Y |  |  |  | Y | Y | Y | Y |
| Tauren | Y | Y |  |  |  |  | Y |  | Y |
| Troll |  | Y |  |  | Y | no? | Y |  | Y |

# Miscellaneous character mechanics ideas

* top-tier/ultimate ability can be picked up from ground (like in Quake or similar arena shooters) and then cast once

## Starting abilities by class

...work in progress...

<table>
<tr>
<th>

</th>
<th>

</th>
<th>

</th>
<th>

</th>
<th>

</th>
</tr>
<tr>
<td>Druid</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td>Hunter</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td>Mage</td>
<td>Blink</td>
<td>Flamestrike</td>
<td>Frostbolt</td>
<td>

</td>
</tr>
<tr>
<td>Paladin</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td>Priest</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td>Rogue</td>
<td>Gouge</td>
<td>Backstab</td>
<td>Eviscerate</td>
<td>Stealth..?</td>
</tr>
<tr>
<td>Shaman</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td>Warlock</td>
<td>Agony</td>
<td>Summon Voidwalker</td>
<td>Incinerate</td>
<td>

</td>
</tr>
<tr>
<td>Warrior</td>
<td>Charge</td>
<td>Slam</td>
<td>

Thunder Clap (or Piercing Howl?)

must be some kind of aoe, not reliant on weapon type
</td>
<td>Taunt, all 3 stances</td>
</tr>
</table>

## Rogue

Abilities which won't be included:

* <span dir="">Distract (tends to be buggy and allows trolling without counterplay)</span>
* <span dir="">Pick Lock (forces rogues into role and removes interesting gameplay avenue from all other classes)</span>
* <span dir="">Cloak of Shadows (no counterplay)</span>
* <span dir="">Evasion (no counterplay)</span>
* <span dir="">Blind (too OP, Gouge is the new Blind)</span>
* <span dir="">Kick (okay in isolation but OP in context of rogue kit)</span>
* <span dir="">Sprint (replaced with Dash, keeps rogue and druid more distinct from each other)</span>
* <span dir="">Dismantle (too ez, Riposte instead)</span>
* <span dir="">Pick Pocket? (loot table population below bottom of dev priority?)</span>

New abilities:

* Dash: (1 charge, 2nd can be talented)<span dir="">Burst forward, breaking free from any immobilization or movement speed reduction effect. For the next 2 sec, your speed is increased by 100%, all movement speed reduction effects are suppressed and reduces the chance ranged attacks hit you by 25%. Can be used while stealthed.</span>

Cheap Shot is ultimate.

## Priest

### Racial abilities

* Human [**_Feedback_**](https://tbc.wowhead.com/spell=13896/feedback): <span dir="">_The priest becomes surrounded with anti-magic energy.  Any successful spell cast against the priest will burn x of the attacker's Mana, causing 1 Shadow damage for each point of Mana burned. Lasts 10 sec._ </span>(no gcd)
  * also passive: **_Desperate Prayer_** heal, cooldown and mana cost increased by 10%.
* Blood Elf [**_Consume Magic_**](https://tbc.wowhead.com/spell=32676/consume-magic): <span dir="">_Dispels one beneficial Magic effect from the caster and gives them x to y mana. The dispelled effect must be a priest spell._</span>
* Draenei (improved version of PW:Fortitude?)
* Dwarf [**_Chastise_**](https://tbc.wowhead.com/spell=44041/chastise): _Chastise the target, causing x to y Holy damage and Immobilizing them for up to 2 sec. Only works against Humanoids._ (0.5 sec cast, 0.5 sec gcd, ...original cldwn 30sec...)
* Night Elf **_Starfall:_** <span dir="">_Rains down moonlight upon targeted area, burning all enemies for x to y Arcane damage every 2 sec_</span>_, increasing **all** healing received by 25% and increasing **all** spell damage by 10%. Lasts 8 sec._ (channeled, large area, small to moderate dmg, significant (1 hour?) cldwn)
* Troll [**_Hex of Weakness_**](https://tbc.wowhead.com/spell=9035/hex-of-weakness): <span dir="">_Weakens the target enemy, reducing damage caused by x and reducing the effectiveness of any healing by 20%. Lasts x sec._ </span>(cldwn?)
